import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class ParenMatch 
{

    public static void main(String[] args) throws IOException 
    {
        // read contents of first argument into string
        String s = new String(Files.readAllBytes(Paths.get(args[0])));
        if (parensMatch(s)) 
        {
            System.out.println("Balanced.");
        } 
        else 
        {
            System.out.println("Imbalanced!");
        }
    }

    public static boolean parensMatch(String s) 
    {
        StackInterface stack = new Stack(); 
        boolean matches = true;
        for(int i = 0; i < s.length(); i++)
        {
            char n = s.charAt(i);
            if(n == '(')
            {
                stack.push("(");
            }
            else if (n == ')')
            {
                String m = stack.pop();
                if( m.equals("("))
                {
                    
                }
                else
                {
                    matches == false;
                }
            }
            
        }
        return matches;
    }

}
